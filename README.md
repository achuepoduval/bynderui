# BynderUI

Test Scenarios

Feature: test login page functionalities

 Background : User is navigated to the login page

  Scenario: login using correct credentials, logout and enter a username and password not registered and validate the error message  populated
    Given the user sees byndertitle
    When the user enters email
    And user enters password
    And user clicks on loginbutton
    Then user verifies the url content in dashboard page
    And user validates the profilename in the dashboard page
    When user clicks on the profilename
    And user clicks on logout button
    Then user is on the login page
    And user verifies the text content in login page url
    When user enters an invalid username
    And user enters an invalid password
    Then user clicks on login button
    And user is sees a message saying you have entered wrong username and password

  Scenario: On clicking lost password button user gets navigated to forgot password page and on clicking cancel button user gets back to login page
    When user clicks on lost password button
    And user is navigated to forgotpassword page
    Then user verifies the text content in the forgot password page url
    When user clicks on cancel button
    Thhn user is navigated back to login page
    And user user verifies the text content in the login page url

  Scenario: user enters only username and no password, click on login
    When user enters the username field
    And user clicks on login button
    Then user verifies the error message displayed saying enter password

 Scenario: user enters only password and no username, click on login
    When user enters password
    And user clicks on login button
    Then user is navigated to the security page

Scenario Outline: Change language in dropdown and validate whether login and lost passwords text contents are updated accordingly
    When user clicks on language button
    And user selects the <language>
    Then the lost password button text is displayed as <textlostpassword>
    Then the login button text is displayed as <logintext>

    Examples:

    | Language      | textlostpassword                          | logintext   |
    |Nederland      | Wachtwoord vergeten?                      | Inloggen    |
    |France         | Vous avez perdu votre mot de passe ?      | Connexion   |
    |Deutschland    | Passwort vergessen?                       | Login       |
    |Italia         | Hai perso la password?                    | Login       |
    |España         | ¿Has olvidado la contraseña?              | Conexión    |
    |United States  | Lost password?                            | Login       |

Run tests in headful mode: npm run test


Run tests in headless mode: npm run test:headless

Run test in Docker container: docker run -it --platform linux/amd64 -v ${PWD}:/bynderuitest -w /bynderuitest cypress/included:9.7.0  —spec cypress/integration/login.spec.js —browser chrome

Run test in a container from the dockerfile : docker run -it <docker image built from dockerfile> test:headless
Prerequisites: Install Docker, Build docker image, Use the docker image in the above command 

